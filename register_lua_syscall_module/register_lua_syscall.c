/*-
 * Copyright (c) 1999 Assar Westerlund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: releng/10.1/share/examples/kld/syscall/module/syscall.c 193374 2009-06-03 09:28:58Z pjd $
 */

#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/file.h>
#include <sys/filedesc.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/capability.h>
#include <machine/reg.h>
#include <machine/pcb.h>

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

/* Magic number */
#define DTYPE_LUAJIT	13

struct luafd {
	lua_State *L;
	struct thread *owner;
	struct callout c;
};

static int
invfo_rdwr(struct file *fp, struct uio *uio, struct ucred *active_cred, 
	int flags, struct thread *td)
{
	return (EOPNOTSUPP);
}

static int
invfo_truncate(struct file *fp, off_t length, struct ucred *active_cred,
	struct thread *td)
{
	return (EINVAL);
}

static int
invfo_ioctl(struct file *fp, u_long com, void *data,
	struct ucred *active_cred, struct thread *td)
{
	return (ENOTTY);
}

static int
invfo_poll(struct file *fp, int events, struct ucred *active_cred,
	struct thread *td)
{
	return (poll_no_poll(events));
}

static int
invfo_kqfilter(struct file *fp, struct knote *kn)
{
	return (EINVAL);
}

static int
invfo_stat(struct file *fp, struct stat *sb, struct ucred *active_cred, 
	struct thread *td)
{
	return (EOPNOTSUPP);
}

static int
luastate_close(struct file *fp, struct thread *td)
{
	struct luafd *lfd = fp->f_data;

	lua_State *L = lfd->L;

	fp->f_ops = &badfileops;
	fp->f_data = NULL;

	if (L) {
		lua_close(L);
	}

	callout_stop(&lfd->c);
	free(lfd, M_TEMP);

	return (0);
}

static struct fileops luajitops = {
	.fo_read = invfo_rdwr,
	.fo_write = invfo_rdwr,
	.fo_truncate = invfo_truncate,
	.fo_ioctl = invfo_ioctl,
	.fo_poll = invfo_poll,
	.fo_kqfilter = invfo_kqfilter,
	.fo_stat = invfo_stat,
	.fo_close = luastate_close,
	.fo_chmod = invfo_chmod,
	.fo_chown = invfo_chown,
	.fo_sendfile = invfo_sendfile,
	.fo_flags = 0
};

/* ------------------------------------------------------------------- */

static void lua_hook(lua_State *L, lua_Debug *ar)
{
	luaL_error(L, "Too long for loading a script");
}

static void script_timeout(void *arg)
{
	lua_State *L = arg;
	lua_sethook(L, lua_hook, LUA_MASKCOUNT, 1);
}

struct register_lua_syscall_args {
	void *code_buf;
	size_t nbytes;
};

/*
 * The function for implementing the syscall.
 */
static int
register_lua_syscall(struct thread *td, void *arg)
{
	int error = 0;
	struct luafd *lfd = malloc(sizeof(struct luafd), M_TEMP, M_NOWAIT);
	if (lfd == NULL) {
		return (ENOMEM);
	}

	struct callout *c = &lfd->c;

	struct register_lua_syscall_args *uap = (struct register_lua_syscall_args *)arg;
	size_t buf_size = uap->nbytes;

	if (buf_size <= 0) {
		return (EINVAL);
	}

	char *code = malloc(buf_size, M_TEMP, M_NOWAIT);
	if (code == NULL) {
		free(lfd, M_TEMP);
		return (EINVAL);
	}

	/* XXX: Take care of the last param later */
	copyinstr(uap->code_buf, code, buf_size, NULL);

	if (code[buf_size - 1] != '\0') {
		free(code, M_TEMP);
		return (EINVAL);
	}

	lua_State *L = luaL_newstate();
	if (L == NULL) {
		free(lfd, M_TEMP);
		free(code, M_TEMP);
		return (ENOMEM);
	}

	luaopen_bit(L);
	luaopen_table(L);

	error = luaL_loadstring(L, code);
	if (error) {
		printf("An error occurs: %s\n", lua_tostring(L, -1));
		lua_close(L);
		free(lfd, M_TEMP);
		free(code, M_TEMP);
		return (EDOOFUS);
	}

	callout_init(c, 0);
	/* Timeout: about 3 seconds */
	callout_reset_curcpu(c, hz * 3, script_timeout, L);

	error = lua_pcall(L, 0, 0, 0);
	if (error) {
		printf("An error occurs: %s\n", lua_tostring(L, -1));
		lua_close(L);
		callout_stop(c);
		free(lfd, M_TEMP);
		free(code, M_TEMP);
		return (EDOOFUS);
	}
	callout_stop(c);

	luaopen_base(L);
	luaopen_syscall(L);
	luaopen_util(L);
#ifndef NO_JIT
	luaopen_jit(L);
#endif

	lfd->L = L;
	lfd->owner = td;

	struct file *fp;
	int fd;

	error = falloc(td, &fp, &fd, 0);
	if (error != 0) {
		lua_close(lfd->L);
		free(lfd, M_TEMP);
		free(code, M_TEMP);
		return (error);
	}

	finit(fp, 0, DTYPE_LUAJIT, lfd, &luajitops);
	td->td_retval[0] = fd;

	fdrop(fp, td);

	free(code, M_TEMP);

	return (0);
}

static struct sysent register_lua_syscall_sysent = {
	2,			/* sy_narg */
	register_lua_syscall			/* sy_call */
};

/*
 * The offset in sysent where the syscall is allocated.
 */
static int offset = NO_SYSCALL;

/*
 * The function called at load/unload.
 */
static int
load(struct module *module, int cmd, void *arg)
{
	int error = 0;

	switch (cmd) {
	case MOD_LOAD :
		printf("register_lua_syscall loaded at %d\n", offset);
		break;
	case MOD_UNLOAD :
		printf("register_lua_syscall unloaded from %d\n", offset);
		break;
	default :
		error = EOPNOTSUPP;
		break;
	}
	return (error);
}

SYSCALL_MODULE(register_lua_syscall, &offset, &register_lua_syscall_sysent, load, NULL);
MODULE_VERSION(register_lua_syscall, 1);
MODULE_DEPEND(register_lua_syscall, luajit_kernel, 1, 1, 1);
