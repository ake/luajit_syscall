/*-
 * Copyright (c) 1999 Assar Westerlund
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * $FreeBSD: releng/10.1/share/examples/kld/syscall/module/syscall.c 193374 2009-06-03 09:28:58Z pjd $
 */

#include <sys/param.h>
#include <sys/proc.h>
#include <sys/module.h>
#include <sys/sysproto.h>
#include <sys/sysent.h>
#include <sys/file.h>
#include <sys/filedesc.h>
#include <sys/kernel.h>
#include <sys/systm.h>
#include <sys/capability.h>
#include <sys/ptrace.h>
#include <sys/sched.h>

#include "lua.h"
#include "lauxlib.h"
#include "lualib.h"

#include "lj_obj.h"

/* Magic number */
#define DTYPE_LUAJIT	13

struct luafd {
	lua_State *L;
	struct thread *owner;
	struct callout c;
};

static void lua_hook(lua_State *L, lua_Debug *ar)
{
	luaL_error(L, "Too long for running a script");
}

struct to_arg {
	struct thread *td;
	lua_State *L;
};

static void script_timeout(void *arg)
{
	printf("Time out occurs\n");
	struct luafd *lfd = arg;
	if (lfd->L->patchpoint != NULL && lfd->L->exitpoint != NULL) {
		uint8_t *p = lfd->L->patchpoint;
		for (; p < lfd->L->exitpoint; p++) {
			*p = 0x90; /* NOP instruction */
		}
	}
	lua_sethook(lfd->L, lua_hook, LUA_MASKCOUNT, 1);
}

/* 
 * XXX: Still not sure if it should accepts an array instead. I want to
 * avoid copying
 */
struct lua_syscall_args {
	int fd;
	size_t nargs;
	ptrdiff_t arg0;
	ptrdiff_t arg1;
	ptrdiff_t arg2;
	ptrdiff_t arg3;
	ptrdiff_t arg4;
	ptrdiff_t arg5;
	ptrdiff_t arg6;
	ptrdiff_t arg7;
	ptrdiff_t arg8;
	ptrdiff_t arg9;
};

static int
lua_syscall(struct thread *td, void *arg)
{
	int error = 0;

	struct lua_syscall_args *uap = (struct lua_syscall_args *)arg;
	int fd = uap->fd;
	size_t nargs = uap->nargs;

	if (nargs > 11) {
		return (EINVAL);
	}

	ptrdiff_t lua_args[10] = {0};
	lua_args[0] = uap->arg0;
	lua_args[1] = uap->arg1;
	lua_args[2] = uap->arg2;
	lua_args[3] = uap->arg3;
	lua_args[4] = uap->arg4;
	lua_args[5] = uap->arg5;
	lua_args[6] = uap->arg6;
	lua_args[7] = uap->arg7;
	lua_args[8] = uap->arg8;
	lua_args[9] = uap->arg9;

	cap_rights_t rights;
	struct file *fp;

	error = fget(td, fd, cap_rights_init(&rights), &fp);
	if (error != 0) {
		return (error);
	}
	
	if (fp->f_type != DTYPE_LUAJIT) {
		fdrop(fp, td);
		return (EBADF);
	}

	struct luafd *lfd = fp->f_data;
	lua_State *L = lfd->L;
	struct callout *c = &lfd->c;

	lua_getglobal(L, "run");
	if (lua_isnil(L, -1)) {
		printf("An error occurs: unknown variable or function\n");
		fdrop(fp, td);
		return (EDOOFUS);
	}
	
	unsigned int i;
	for (i = 0; i < nargs; i++) {
		lua_pushinteger(L, lua_args[i]);
	}

	/* Timeout: about 3 seconds */
	callout_reset(c, hz * 3, script_timeout, lfd);

#ifdef USE_LUA_CALL
	lua_call(L, nargs, 1);
#else
	error = lua_pcall(L, nargs, 1, 0);
	if (error != 0) {
		printf("An error occurs when running the function run(): %s\n", lua_tostring(L, -1));
		callout_stop(c);
		lua_settop(L, 0);
		fdrop(fp, td);
		return (EDOOFUS);
	}
#endif
	callout_stop(c);

	int result = lua_tointeger(L, -1);
	td->td_retval[0] = result;

	lua_settop(L, 0);
	
	fdrop(fp, td);
	return (0);
}

/*
 * The `sysent' for the new syscall
 */
static struct sysent lua_syscall_sysent = {
	12,			/* sy_narg */
	lua_syscall			/* sy_call */
};

/* ------------------------------------------------------------------- */

/*
 * The offset in sysent where the syscall is allocated.
 */
static int offset = NO_SYSCALL;

/*
 * The function called at load/unload.
 */
static int
load(struct module *module, int cmd, void *arg)
{
	int error = 0;

	switch (cmd) {
	case MOD_LOAD :
		printf("lua_syscall loaded at %d\n", offset);
		break;
	case MOD_UNLOAD :
		printf("lua_syscall unloaded from %d\n", offset);
		break;
	default :
		error = EOPNOTSUPP;
		break;
	}
	return (error);
}

SYSCALL_MODULE(lua_syscall, &offset, &lua_syscall_sysent, load, NULL);
MODULE_VERSION(lua_syscall, 1);
MODULE_DEPEND(lua_syscall, luajit_kernel, 1, 1, 1);
MODULE_DEPEND(lua_syscall, register_lua_syscall, 1, 1, 1);
